﻿using System.Drawing;
using AForge.Controls;
using VideoProcessor.Controls;

namespace VideoProcessor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openVideofileusingDirectShowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openVideoVwfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openMjpegToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.videoInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.videoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filtersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.metricsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.frameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveOriginalFrameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveProcessedFrameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.fpsLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.sourceVideoPanel = new System.Windows.Forms.Panel();
            this.videoPlayer = new AForge.Controls.VideoSourcePlayer();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.processedVideoPanel = new System.Windows.Forms.Panel();
            this.processedPicture = new System.Windows.Forms.PictureBox();
            this.labelTimeProgressStart = new System.Windows.Forms.Label();
            this.panelVideoProgress = new System.Windows.Forms.Panel();
            this.labelTimeProgressFinish = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.buttonPlay = new System.Windows.Forms.PictureBox();
            this.progressBar = new VideoProcessor.Controls.UpdatableProgressBar();
            this.labelFrameNumber = new System.Windows.Forms.Label();
            this.mainMenuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.sourceVideoPanel.SuspendLayout();
            this.processedVideoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.processedPicture)).BeginInit();
            this.panelVideoProgress.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonPlay)).BeginInit();
            this.SuspendLayout();
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.videoToolStripMenuItem,
            this.frameToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(1023, 24);
            this.mainMenuStrip.TabIndex = 0;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openVideofileusingDirectShowToolStripMenuItem,
            this.openVideoVwfToolStripMenuItem,
            this.openMjpegToolStripMenuItem,
            this.toolStripSeparator1,
            this.videoInfoToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openVideofileusingDirectShowToolStripMenuItem
            // 
            this.openVideofileusingDirectShowToolStripMenuItem.Name = "openVideofileusingDirectShowToolStripMenuItem";
            this.openVideofileusingDirectShowToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.openVideofileusingDirectShowToolStripMenuItem.Text = "Open video &file (using DirectShow)";
            this.openVideofileusingDirectShowToolStripMenuItem.Click += new System.EventHandler(this.openVideofileusingDirectShowToolStripMenuItem_Click);
            // 
            // openVideoVwfToolStripMenuItem
            // 
            this.openVideoVwfToolStripMenuItem.Name = "openVideoVwfToolStripMenuItem";
            this.openVideoVwfToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.openVideoVwfToolStripMenuItem.Text = "Open video file (using VFW)";
            this.openVideoVwfToolStripMenuItem.Click += new System.EventHandler(this.openVideofileusingDirectShowToolStripMenuItem_Click);
            // 
            // openMjpegToolStripMenuItem
            // 
            this.openMjpegToolStripMenuItem.Name = "openMjpegToolStripMenuItem";
            this.openMjpegToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.openMjpegToolStripMenuItem.Text = "Open mjpeg";
            this.openMjpegToolStripMenuItem.Click += new System.EventHandler(this.openMjpegToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(254, 6);
            // 
            // videoInfoToolStripMenuItem
            // 
            this.videoInfoToolStripMenuItem.Enabled = false;
            this.videoInfoToolStripMenuItem.Name = "videoInfoToolStripMenuItem";
            this.videoInfoToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.videoInfoToolStripMenuItem.Text = "Video Info";
            this.videoInfoToolStripMenuItem.Click += new System.EventHandler(this.videoInfoToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(254, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // videoToolStripMenuItem
            // 
            this.videoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filtersToolStripMenuItem,
            this.metricsToolStripMenuItem});
            this.videoToolStripMenuItem.Name = "videoToolStripMenuItem";
            this.videoToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.videoToolStripMenuItem.Text = "Video";
            // 
            // filtersToolStripMenuItem
            // 
            this.filtersToolStripMenuItem.Name = "filtersToolStripMenuItem";
            this.filtersToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.filtersToolStripMenuItem.Text = "Effects";
            this.filtersToolStripMenuItem.Click += new System.EventHandler(this.filtersToolStripMenuItem_Click);
            // 
            // metricsToolStripMenuItem
            // 
            this.metricsToolStripMenuItem.Name = "metricsToolStripMenuItem";
            this.metricsToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.metricsToolStripMenuItem.Text = "Metrics";
            this.metricsToolStripMenuItem.Click += new System.EventHandler(this.metricsToolStripMenuItem_Click);
            // 
            // frameToolStripMenuItem
            // 
            this.frameToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveOriginalFrameToolStripMenuItem,
            this.saveProcessedFrameToolStripMenuItem});
            this.frameToolStripMenuItem.Name = "frameToolStripMenuItem";
            this.frameToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.frameToolStripMenuItem.Text = "Frame";
            // 
            // saveOriginalFrameToolStripMenuItem
            // 
            this.saveOriginalFrameToolStripMenuItem.Name = "saveOriginalFrameToolStripMenuItem";
            this.saveOriginalFrameToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.saveOriginalFrameToolStripMenuItem.Text = "Save original frame";
            this.saveOriginalFrameToolStripMenuItem.Click += new System.EventHandler(this.saveOriginalFrameToolStripMenuItem_Click);
            // 
            // saveProcessedFrameToolStripMenuItem
            // 
            this.saveProcessedFrameToolStripMenuItem.Name = "saveProcessedFrameToolStripMenuItem";
            this.saveProcessedFrameToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.saveProcessedFrameToolStripMenuItem.Text = "Save processed frame";
            this.saveProcessedFrameToolStripMenuItem.Click += new System.EventHandler(this.saveProcessedFrameToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fpsLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 332);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1023, 22);
            this.statusStrip.TabIndex = 1;
            this.statusStrip.Text = "statusStrip1";
            // 
            // fpsLabel
            // 
            this.fpsLabel.Name = "fpsLabel";
            this.fpsLabel.Size = new System.Drawing.Size(1008, 17);
            this.fpsLabel.Spring = true;
            this.fpsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sourceVideoPanel
            // 
            this.sourceVideoPanel.Controls.Add(this.videoPlayer);
            this.sourceVideoPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.sourceVideoPanel.Location = new System.Drawing.Point(0, 24);
            this.sourceVideoPanel.Name = "sourceVideoPanel";
            this.sourceVideoPanel.Size = new System.Drawing.Size(535, 284);
            this.sourceVideoPanel.TabIndex = 2;
            // 
            // videoPlayer
            // 
            this.videoPlayer.AutoSizeControl = true;
            this.videoPlayer.BackColor = System.Drawing.SystemColors.Control;
            this.videoPlayer.BorderColor = System.Drawing.SystemColors.Control;
            this.videoPlayer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.videoPlayer.ForeColor = System.Drawing.Color.White;
            this.videoPlayer.KeepAspectRatio = true;
            this.videoPlayer.Location = new System.Drawing.Point(0, 0);
            this.videoPlayer.Name = "videoPlayer";
            this.videoPlayer.Size = new System.Drawing.Size(535, 284);
            this.videoPlayer.TabIndex = 0;
            this.videoPlayer.VideoSource = null;
            this.videoPlayer.NewFrame += new AForge.Controls.VideoSourcePlayer.NewFrameHandler(this.videoSourcePlayer_NewFrame);
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Video files|*.avi;*.mp4|All files (*.*)|*.*";
            this.openFileDialog.Title = "Opem movie";
            // 
            // processedVideoPanel
            // 
            this.processedVideoPanel.Controls.Add(this.processedPicture);
            this.processedVideoPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.processedVideoPanel.Location = new System.Drawing.Point(535, 24);
            this.processedVideoPanel.Name = "processedVideoPanel";
            this.processedVideoPanel.Size = new System.Drawing.Size(488, 284);
            this.processedVideoPanel.TabIndex = 3;
            // 
            // processedPicture
            // 
            this.processedPicture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.processedPicture.Location = new System.Drawing.Point(0, 0);
            this.processedPicture.Name = "processedPicture";
            this.processedPicture.Size = new System.Drawing.Size(488, 284);
            this.processedPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.processedPicture.TabIndex = 0;
            this.processedPicture.TabStop = false;
            this.processedPicture.Click += new System.EventHandler(this.processedPicture_Click);
            // 
            // labelTimeProgressStart
            // 
            this.labelTimeProgressStart.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTimeProgressStart.Location = new System.Drawing.Point(71, 0);
            this.labelTimeProgressStart.Name = "labelTimeProgressStart";
            this.labelTimeProgressStart.Size = new System.Drawing.Size(60, 24);
            this.labelTimeProgressStart.TabIndex = 4;
            this.labelTimeProgressStart.Text = "--:--";
            this.labelTimeProgressStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelVideoProgress
            // 
            this.panelVideoProgress.Controls.Add(this.labelFrameNumber);
            this.panelVideoProgress.Controls.Add(this.progressBar);
            this.panelVideoProgress.Controls.Add(this.labelTimeProgressFinish);
            this.panelVideoProgress.Controls.Add(this.labelTimeProgressStart);
            this.panelVideoProgress.Controls.Add(this.panel1);
            this.panelVideoProgress.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelVideoProgress.Location = new System.Drawing.Point(0, 308);
            this.panelVideoProgress.Name = "panelVideoProgress";
            this.panelVideoProgress.Size = new System.Drawing.Size(1023, 24);
            this.panelVideoProgress.TabIndex = 5;
            // 
            // labelTimeProgressFinish
            // 
            this.labelTimeProgressFinish.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelTimeProgressFinish.Location = new System.Drawing.Point(963, 0);
            this.labelTimeProgressFinish.Name = "labelTimeProgressFinish";
            this.labelTimeProgressFinish.Size = new System.Drawing.Size(60, 24);
            this.labelTimeProgressFinish.TabIndex = 5;
            this.labelTimeProgressFinish.Text = "--:--";
            this.labelTimeProgressFinish.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.buttonPlay);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(71, 24);
            this.panel1.TabIndex = 6;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(37, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(33, 23);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // buttonPlay
            // 
            this.buttonPlay.Image = ((System.Drawing.Image)(resources.GetObject("buttonPlay.Image")));
            this.buttonPlay.Location = new System.Drawing.Point(2, 0);
            this.buttonPlay.Name = "buttonPlay";
            this.buttonPlay.Size = new System.Drawing.Size(33, 23);
            this.buttonPlay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.buttonPlay.TabIndex = 0;
            this.buttonPlay.TabStop = false;
            this.buttonPlay.Click += new System.EventHandler(this.buttonPlay_Click);
            // 
            // progressBar
            // 
            this.progressBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar.Location = new System.Drawing.Point(131, 0);
            this.progressBar.MarqueeAnimationSpeed = 0;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(832, 24);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar.TabIndex = 1;
            this.progressBar.ValueChanged += new VideoProcessor.Controls.UpdatableProgressBar.ValueChange(this.progressBar_ValueChanged);
            // 
            // labelFrameNumber
            // 
            this.labelFrameNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelFrameNumber.AutoSize = true;
            this.labelFrameNumber.Location = new System.Drawing.Point(525, 6);
            this.labelFrameNumber.Name = "labelFrameNumber";
            this.labelFrameNumber.Size = new System.Drawing.Size(13, 13);
            this.labelFrameNumber.TabIndex = 7;
            this.labelFrameNumber.Text = "0";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1023, 354);
            this.Controls.Add(this.processedVideoPanel);
            this.Controls.Add(this.sourceVideoPanel);
            this.Controls.Add(this.panelVideoProgress);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.mainMenuStrip);
            this.MainMenuStrip = this.mainMenuStrip;
            this.Name = "MainForm";
            this.Text = "Video Processing";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Shown += new System.EventHandler(this.MainForm_SizeChanged);
            this.SizeChanged += new System.EventHandler(this.MainForm_SizeChanged);
            this.Resize += new System.EventHandler(this.MainForm_SizeChanged);
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.sourceVideoPanel.ResumeLayout(false);
            this.processedVideoPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.processedPicture)).EndInit();
            this.panelVideoProgress.ResumeLayout(false);
            this.panelVideoProgress.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonPlay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.Panel sourceVideoPanel;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private AForge.Controls.VideoSourcePlayer videoPlayer;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ToolStripStatusLabel fpsLabel;
        private System.Windows.Forms.ToolStripMenuItem openVideofileusingDirectShowToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Panel processedVideoPanel;
        private System.Windows.Forms.PictureBox processedPicture;
        private System.Windows.Forms.ToolStripMenuItem videoInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openVideoVwfToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private UpdatableProgressBar progressBar;
        private System.Windows.Forms.Label labelTimeProgressStart;
        private System.Windows.Forms.Panel panelVideoProgress;
        private System.Windows.Forms.Label labelTimeProgressFinish;
        private System.Windows.Forms.ToolStripMenuItem frameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveOriginalFrameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveProcessedFrameToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox buttonPlay;
        private System.Windows.Forms.ToolStripMenuItem videoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filtersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem metricsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openMjpegToolStripMenuItem;
        private System.Windows.Forms.Label labelFrameNumber;
    }
}


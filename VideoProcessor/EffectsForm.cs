﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using VideoProcessor.Algorithms;
using VideoProcessor.Helpers;
using VideoProcessor.Model;
using VideoProcessor.MotionDetector;

// ReSharper disable InconsistentNaming

namespace VideoProcessor {
    public partial class EffectsForm : Form
    {
        private readonly ImageProcessor _imageProcessor;
        private readonly Frame[] _frames;

        private float _gammaCorrectionValue;
        private int _logCorrectionValue;

        private ProcessTypeEnum _processType;
        private ColorModelEnum _colorModel;
        private int _frameNumber;

        private int _linearAverageRadiusValue;
        private int _medianFilterRadius;
        private int _2DCLeanerRadiusValue;
        private int _2DCLeanerThresholdValue;
        private int _backgroundSubstractorThreshold;
        private int _sceneChangeDetectorThreshold;
        private int _vocrContourThreshold;
        private float _vocrContourGain;
        private int _vocrContourBrightnessThreshold;
        private bool _vocrContourShow;


        private int _blockThreshold;
        private int _blockSize;

        private BackgroundSubstractor _backgroundSubstractor;
        private FeaturePointMotionDetector _feautePointMotionDetector;
        private SceneChangeDetector _sceneChangeDetector;
        private BlockMatchingDetector _blockMatchingDetector;

        public EffectsForm(Frame[] frames) {
            InitializeComponent();
            _frames = frames;
            _imageProcessor = new ImageProcessor();

            trackBarGammaCorrection.Setup(checkBoxGammaCorrection, "Gamma Correction (gamma = {0:f}",
                value => _gammaCorrectionValue = value,
                value => value / 10f);

            trackBarLogCorrection.Setup(checkBoxLogCorrection, "Log Correction (k = {0:f}",
                value => _logCorrectionValue = (int)value);

            trackBarFrameNumber.Setup(labelFrameNumber, "Frame number = {0}",
                value => _frameNumber = (int)value);

            trackBarLinearAverageRadius.Setup(labelLinearAverageRadius, "Radius R = {0}",
                value => _linearAverageRadiusValue = (int)value);

            trackBar2DCleanerRadius.Setup(label2DCleanerRadius, "Radius R = {0}",
                value => _2DCLeanerRadiusValue = (int)value);

            trackBar2DCleanerThreshold.Setup(label2DCleanerThreshold, "Threshold = {0}",
                value => _2DCLeanerThresholdValue = (int)value);

            trackBarMedianFilterRadius.Setup(labelMedianFilterRadius, "Radius R = {0}",
                value => _medianFilterRadius = (int)value);

            trackBarBackgroundSubstractorThreshold.Setup(labelBackroundSubstractorThreshold, "Threshold = {0}",
                value => _backgroundSubstractorThreshold = (int)value);

            trackBarSceneChangeDetectorThreshold.Setup(labelSceneChangeDetectorThreshold, "Threshold = {0}",
                value => _sceneChangeDetectorThreshold = (int)value);

            trackBarBlockThreshold.Setup(labelBlockThreshold, "Threshold = {0}",
                value => _blockThreshold = (int)value);

            trackBarBlockSize.Setup(labelBlockSize, "Block Size = {0}",
                value => _blockSize = (int)value);

            trackBarVocrControurThreshold.Setup(labelVocrControurThreshold, "Threshold = {0}",
                value => _vocrContourThreshold = (int)value);

            trackBarVocrControurGain.Setup(labelVocrControurGain, "Gain = {0}",
                value => _vocrContourGain = value, i => i / 100f);

            trackBarVocrControurBrightnessThreshold.Setup(labelVocrControurBrightnessThreshold, "Brightness threshold = {0}",
                value => _vocrContourBrightnessThreshold = (int)value);

            checkBoxVocrContourShow.Setup(value => _vocrContourShow = value);

            radioButtonProcessTypeParallelepiped.CheckedChanged +=  ProcessTypeRadioButtonHandler;
            radioButtonProcessTypePyramid.CheckedChanged +=  ProcessTypeRadioButtonHandler;
            radioButtonProcessTypeCone.CheckedChanged +=  ProcessTypeRadioButtonHandler;
            radioButtonColorModelRgb.CheckedChanged += ColorModelRadioButtonHandler;
            radioButtonColorModelYuv.CheckedChanged += ColorModelRadioButtonHandler;

            _backgroundSubstractor = new BackgroundSubstractor();
            _feautePointMotionDetector = new FeaturePointMotionDetector();
            _sceneChangeDetector = new SceneChangeDetector();
            _blockMatchingDetector = new BlockMatchingDetector();

            chartMotionDetector.SetCheckBoxes(checkBoxObjectCountColumn, checkBoxMinAreaColumn, checkBoxMaxAreaColumn);
            chartFeaturePoints.SetCheckBoxes(new []{checkBoxFeaturePointColumn, checkBoxSimilarPointColumn});
        }

        public void Reset()
        {
            _backgroundSubstractor = new BackgroundSubstractor();
            _feautePointMotionDetector = new FeaturePointMotionDetector();
            _sceneChangeDetector = new SceneChangeDetector();
            _blockMatchingDetector = new BlockMatchingDetector();
        }

        public Image Process()
        {
            var frame = _frames.First().Copy();

            if (checkBoxGrayScale.Checked) {
                _imageProcessor.AlgorithmGrayScale(frame, GrayScale.FromRgb);
            }

            if (checkBoxGammaCorrection.Checked) {
                _imageProcessor.AlgorithmGammaCorrection(frame, _gammaCorrectionValue);
            }

            if (checkBoxLogCorrection.Checked) {
                _imageProcessor.AlgorithmLogCorrection(frame, _logCorrectionValue);
            }

            if (checkBoxLinearAverage.Checked) {
                _imageProcessor.FilterLinearAverage(frame, _frames, _frameNumber, _linearAverageRadiusValue, _colorModel, _processType);
            }

            if (checkBoxMedianFilter.Checked) {
                _imageProcessor.FilterMedian(frame, _frames, _frameNumber, _medianFilterRadius, _colorModel, _processType);
            }

            if (checkBox2DCleaner.Checked) {
                _imageProcessor.Filter2DCleaner(frame, _frames, _2DCLeanerRadiusValue, _2DCLeanerThresholdValue);
            }

            DetectorResult detectorResult = null;
            DetectorResult textDetectorResult = null;
            MKeyPoint[] points = null;

            if (checkBoxVocrContour.Checked) {
                textDetectorResult = _imageProcessor.VocrControur(frame, _vocrContourThreshold, _vocrContourGain, _vocrContourBrightnessThreshold, _vocrContourShow);
            }

            if (checkBoxUsingColorInfromation.Checked)
            {
                textDetectorResult = _imageProcessor.VocrColorInformation(frame, _vocrContourThreshold, _vocrContourGain, _vocrContourBrightnessThreshold, _vocrContourShow);
                //points = _imageProcessor.VocrColorInformation(frame, _vocrContourThreshold, _vocrContourGain, _vocrContourBrightnessThreshold, _vocrContourShow);
            }
            
            if (checkBoxBackgroundSubtraction.Checked) {
                detectorResult = _backgroundSubstractor.Process(frame, _backgroundSubstractorThreshold);
            }

            if (checkBoxFeatureMotionDetector.Checked) {
                detectorResult = _feautePointMotionDetector.Process(frame.Copy(true), _frames[1].Copy(true));
            }

            if (checkBoxBlockMatchingDetector.Checked) {
                detectorResult = _blockMatchingDetector.Process(frame.Copy(true), _blockSize, _blockThreshold);
            }

            frame.SaveChanges();

            using (Graphics g = Graphics.FromImage(frame.Image))
            {
                Pen pen = new Pen(Color.Green, 3);
                Pen pen2 = new Pen(Color.Red);
                if (checkBoxSceneChangeDetector.Checked) {
                    _sceneChangeDetector.Process(frame, _sceneChangeDetectorThreshold, sceneNumber => {
                        Brush brush = new SolidBrush(Color.Red);
                        g.DrawString("Scene " + sceneNumber, new Font(FontFamily.GenericSansSerif, 30), brush, 10, 30);
                    });
                }

                if (textDetectorResult != null)
                {
                    var totalTextRegions = textDetectorResult.Regions.Count;
                    var regions = textDetectorResult.Regions
                        .Where(item => item.IsGoodTextRegion)
                        .OrderByDescending(item => item.Area)
                        .ToArray();


                    //All regions
                    foreach (var region in regions)
                    {
                        g.DrawRectangle(pen2, region.Rectangle);
                    }

                    //Good regions
                    for (int regIndex = 0; regIndex < regions.Length; regIndex++) {
                        var region = regions[regIndex];
                        //если уже был отрисован прямоугольник, пересекающийся с данным, то пропустить
                        if (regIndex > 0 && regions.Take(regIndex).Any(item => item.Rectangle.IntersectsWith(region.Rectangle))) continue;

                        g.DrawRectangle(pen, region.Rectangle);
                    }
                }
                if (points != null) {
                    foreach (var point in points) {
                        g.DrawArc(pen, (int)point.Point.X - 3, (int)point.Point.Y - 3, 6, 6, 0, 360);
                    }
                }
                if (detectorResult != null)
                {
                    var regions = detectorResult.Regions
                        .Where(item => item.IsGoodRegion)
                        .OrderByDescending(item => item.Area)
                        .ToArray();

                    if (checkBoxLog.Checked && regions.Any())
                    {
                        chartMotionDetector.Invoke((MethodInvoker) delegate
                        {
                            var x = chartMotionDetector.Series[0].Points.Count + 1;
                            var min = regions.Min(item => item.Area) / 1000f;
                            var max = regions.Max(item => item.Area) / 1000f;
                            chartMotionDetector.Series[0].Points.AddXY(x, regions.Length);
                            chartMotionDetector.Series[1].Points.AddXY(x, min);
                            chartMotionDetector.Series[2].Points.AddXY(x, max);

                            var log = string.Format("{0}; {1}; {2}; {3}", x, regions.Length, min, max);
                            var featureDetectorResult = detectorResult as FeatureDetectorResult;
                            if (featureDetectorResult != null)
                            {
                                x = chartFeaturePoints.Series[0].Points.Count + 1;
                                chartFeaturePoints.Series[0].Points.AddXY(x, featureDetectorResult.FeaturePointCount);
                                chartFeaturePoints.Series[1].Points.AddXY(x, featureDetectorResult.SimilarPointCount);
                                log += string.Format("; {0}; {1}", featureDetectorResult.FeaturePointCount, featureDetectorResult.SimilarPointCount);
                            }
                            textBoxLog.Text += log + "\r\n";
                        });
                    }

                    for (int regIndex = 0; regIndex < regions.Length; regIndex++) {
                        var region = regions[regIndex];
                        //если уже был отрисован прямоугольник, пересекающийся с данным, то пропустить
                        if (regIndex > 0 && regions.Take(regIndex).Any(item => item.Rectangle.IntersectsWith(region.Rectangle))) continue;

                        g.DrawRectangle(pen, region.Rectangle);
                    }
                }
            }

            return frame.Image;
        }

        private void FilterForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void ProcessTypeRadioButtonHandler(object sender, EventArgs e) {
            if (radioButtonProcessTypeParallelepiped.Checked)
            {
                _processType = ProcessTypeEnum.Parallelepiped;
            }
            else if (radioButtonProcessTypePyramid.Checked)
            {
                _processType = ProcessTypeEnum.Pyramid;
            }
            else
            {
                _processType = ProcessTypeEnum.Cone;
            }
        }

        private void ColorModelRadioButtonHandler(object sender, EventArgs e) {
            _colorModel = radioButtonColorModelRgb.Checked ? ColorModelEnum.Rgb : ColorModelEnum.Yuv;
        }
    }
}

﻿namespace VideoProcessor {
    partial class EffectsForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.checkBoxGammaCorrection = new System.Windows.Forms.CheckBox();
            this.checkBoxGrayScale = new System.Windows.Forms.CheckBox();
            this.trackBarGammaCorrection = new System.Windows.Forms.TrackBar();
            this.checkBoxLogCorrection = new System.Windows.Forms.CheckBox();
            this.trackBarLogCorrection = new System.Windows.Forms.TrackBar();
            this.checkBoxLinearAverage = new System.Windows.Forms.CheckBox();
            this.trackBarLinearAverageRadius = new System.Windows.Forms.TrackBar();
            this.radioButtonColorModelRgb = new System.Windows.Forms.RadioButton();
            this.radioButtonColorModelYuv = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelMedianFilterRadius = new System.Windows.Forms.Label();
            this.checkBoxMedianFilter = new System.Windows.Forms.CheckBox();
            this.trackBarMedianFilterRadius = new System.Windows.Forms.TrackBar();
            this.label2DCleanerThreshold = new System.Windows.Forms.Label();
            this.label2DCleanerRadius = new System.Windows.Forms.Label();
            this.trackBar2DCleanerThreshold = new System.Windows.Forms.TrackBar();
            this.labelLinearAverageRadius = new System.Windows.Forms.Label();
            this.checkBox2DCleaner = new System.Windows.Forms.CheckBox();
            this.trackBar2DCleanerRadius = new System.Windows.Forms.TrackBar();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radioButtonProcessTypeParallelepiped = new System.Windows.Forms.RadioButton();
            this.radioButtonProcessTypePyramid = new System.Windows.Forms.RadioButton();
            this.radioButtonProcessTypeCone = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelFrameNumber = new System.Windows.Forms.Label();
            this.trackBarFrameNumber = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.trackBar3 = new System.Windows.Forms.TrackBar();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.trackBar4 = new System.Windows.Forms.TrackBar();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.trackBar5 = new System.Windows.Forms.TrackBar();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.trackBar6 = new System.Windows.Forms.TrackBar();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.trackBar7 = new System.Windows.Forms.TrackBar();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.checkBoxMaxAreaColumn = new System.Windows.Forms.CheckBox();
            this.checkBoxMinAreaColumn = new System.Windows.Forms.CheckBox();
            this.checkBoxObjectCountColumn = new System.Windows.Forms.CheckBox();
            this.chartMotionDetector = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.checkBoxSimilarPointColumn = new System.Windows.Forms.CheckBox();
            this.checkBoxFeaturePointColumn = new System.Windows.Forms.CheckBox();
            this.chartFeaturePoints = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.labelBlockThreshold = new System.Windows.Forms.Label();
            this.labelBlockSize = new System.Windows.Forms.Label();
            this.trackBarBlockThreshold = new System.Windows.Forms.TrackBar();
            this.trackBarBlockSize = new System.Windows.Forms.TrackBar();
            this.checkBoxBlockMatchingDetector = new System.Windows.Forms.CheckBox();
            this.checkBoxLog = new System.Windows.Forms.CheckBox();
            this.checkBoxBackgroundSubtraction = new System.Windows.Forms.CheckBox();
            this.trackBarBackgroundSubstractorThreshold = new System.Windows.Forms.TrackBar();
            this.checkBoxFeatureMotionDetector = new System.Windows.Forms.CheckBox();
            this.labelBackroundSubstractorThreshold = new System.Windows.Forms.Label();
            this.labelSceneChangeDetectorThreshold = new System.Windows.Forms.Label();
            this.checkBoxSceneChangeDetector = new System.Windows.Forms.CheckBox();
            this.trackBarSceneChangeDetectorThreshold = new System.Windows.Forms.TrackBar();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.trackBarVocrControurGain = new System.Windows.Forms.TrackBar();
            this.labelVocrControurGain = new System.Windows.Forms.Label();
            this.checkBoxVocrContour = new System.Windows.Forms.CheckBox();
            this.trackBarVocrControurBrightnessThreshold = new System.Windows.Forms.TrackBar();
            this.labelVocrControurBrightnessThreshold = new System.Windows.Forms.Label();
            this.trackBarVocrControurThreshold = new System.Windows.Forms.TrackBar();
            this.labelVocrControurThreshold = new System.Windows.Forms.Label();
            this.checkBoxVocrContourShow = new System.Windows.Forms.CheckBox();
            this.checkBoxUsingColorInfromation = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarGammaCorrection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarLogCorrection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarLinearAverageRadius)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarMedianFilterRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2DCleanerThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2DCleanerRadius)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarFrameNumber)).BeginInit();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar5)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar7)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartMotionDetector)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartFeaturePoints)).BeginInit();
            this.tabPage7.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarBlockThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarBlockSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarBackgroundSubstractorThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSceneChangeDetectorThreshold)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVocrControurGain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVocrControurBrightnessThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVocrControurThreshold)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBoxGammaCorrection
            // 
            this.checkBoxGammaCorrection.AutoSize = true;
            this.checkBoxGammaCorrection.Location = new System.Drawing.Point(5, 29);
            this.checkBoxGammaCorrection.Name = "checkBoxGammaCorrection";
            this.checkBoxGammaCorrection.Size = new System.Drawing.Size(113, 17);
            this.checkBoxGammaCorrection.TabIndex = 2;
            this.checkBoxGammaCorrection.Text = "Gamma Correction";
            this.checkBoxGammaCorrection.UseVisualStyleBackColor = true;
            // 
            // checkBoxGrayScale
            // 
            this.checkBoxGrayScale.AutoSize = true;
            this.checkBoxGrayScale.Location = new System.Drawing.Point(6, 6);
            this.checkBoxGrayScale.Name = "checkBoxGrayScale";
            this.checkBoxGrayScale.Size = new System.Drawing.Size(78, 17);
            this.checkBoxGrayScale.TabIndex = 4;
            this.checkBoxGrayScale.Text = "Gray Scale";
            this.checkBoxGrayScale.UseVisualStyleBackColor = true;
            // 
            // trackBarGammaCorrection
            // 
            this.trackBarGammaCorrection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarGammaCorrection.AutoSize = false;
            this.trackBarGammaCorrection.Location = new System.Drawing.Point(6, 52);
            this.trackBarGammaCorrection.Maximum = 40;
            this.trackBarGammaCorrection.Name = "trackBarGammaCorrection";
            this.trackBarGammaCorrection.Size = new System.Drawing.Size(409, 20);
            this.trackBarGammaCorrection.TabIndex = 0;
            this.trackBarGammaCorrection.Value = 10;
            // 
            // checkBoxLogCorrection
            // 
            this.checkBoxLogCorrection.AutoSize = true;
            this.checkBoxLogCorrection.Location = new System.Drawing.Point(5, 77);
            this.checkBoxLogCorrection.Name = "checkBoxLogCorrection";
            this.checkBoxLogCorrection.Size = new System.Drawing.Size(95, 17);
            this.checkBoxLogCorrection.TabIndex = 6;
            this.checkBoxLogCorrection.Text = "Log Correction";
            this.checkBoxLogCorrection.UseVisualStyleBackColor = true;
            // 
            // trackBarLogCorrection
            // 
            this.trackBarLogCorrection.AutoSize = false;
            this.trackBarLogCorrection.Location = new System.Drawing.Point(6, 100);
            this.trackBarLogCorrection.Maximum = 70;
            this.trackBarLogCorrection.Name = "trackBarLogCorrection";
            this.trackBarLogCorrection.Size = new System.Drawing.Size(311, 20);
            this.trackBarLogCorrection.TabIndex = 5;
            this.trackBarLogCorrection.Value = 50;
            // 
            // checkBoxLinearAverage
            // 
            this.checkBoxLinearAverage.AutoSize = true;
            this.checkBoxLinearAverage.Location = new System.Drawing.Point(6, 107);
            this.checkBoxLinearAverage.Name = "checkBoxLinearAverage";
            this.checkBoxLinearAverage.Size = new System.Drawing.Size(98, 17);
            this.checkBoxLinearAverage.TabIndex = 8;
            this.checkBoxLinearAverage.Text = "Linear Average";
            this.checkBoxLinearAverage.UseVisualStyleBackColor = true;
            // 
            // trackBarLinearAverageRadius
            // 
            this.trackBarLinearAverageRadius.AutoSize = false;
            this.trackBarLinearAverageRadius.LargeChange = 1;
            this.trackBarLinearAverageRadius.Location = new System.Drawing.Point(104, 130);
            this.trackBarLinearAverageRadius.Minimum = 1;
            this.trackBarLinearAverageRadius.Name = "trackBarLinearAverageRadius";
            this.trackBarLinearAverageRadius.Size = new System.Drawing.Size(193, 20);
            this.trackBarLinearAverageRadius.TabIndex = 7;
            this.trackBarLinearAverageRadius.Value = 5;
            // 
            // radioButtonColorModelRgb
            // 
            this.radioButtonColorModelRgb.AutoSize = true;
            this.radioButtonColorModelRgb.Checked = true;
            this.radioButtonColorModelRgb.Location = new System.Drawing.Point(3, 3);
            this.radioButtonColorModelRgb.Name = "radioButtonColorModelRgb";
            this.radioButtonColorModelRgb.Size = new System.Drawing.Size(48, 17);
            this.radioButtonColorModelRgb.TabIndex = 9;
            this.radioButtonColorModelRgb.TabStop = true;
            this.radioButtonColorModelRgb.Text = "RGB";
            this.radioButtonColorModelRgb.UseVisualStyleBackColor = true;
            // 
            // radioButtonColorModelYuv
            // 
            this.radioButtonColorModelYuv.AutoSize = true;
            this.radioButtonColorModelYuv.Location = new System.Drawing.Point(58, 3);
            this.radioButtonColorModelYuv.Name = "radioButtonColorModelYuv";
            this.radioButtonColorModelYuv.Size = new System.Drawing.Size(47, 17);
            this.radioButtonColorModelYuv.TabIndex = 10;
            this.radioButtonColorModelYuv.Text = "YUV";
            this.radioButtonColorModelYuv.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelMedianFilterRadius);
            this.groupBox1.Controls.Add(this.checkBoxMedianFilter);
            this.groupBox1.Controls.Add(this.trackBarMedianFilterRadius);
            this.groupBox1.Controls.Add(this.label2DCleanerThreshold);
            this.groupBox1.Controls.Add(this.label2DCleanerRadius);
            this.groupBox1.Controls.Add(this.trackBar2DCleanerThreshold);
            this.groupBox1.Controls.Add(this.labelLinearAverageRadius);
            this.groupBox1.Controls.Add(this.checkBox2DCleaner);
            this.groupBox1.Controls.Add(this.trackBar2DCleanerRadius);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.labelFrameNumber);
            this.groupBox1.Controls.Add(this.trackBarFrameNumber);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.checkBoxLinearAverage);
            this.groupBox1.Controls.Add(this.trackBarLinearAverageRadius);
            this.groupBox1.Location = new System.Drawing.Point(5, 137);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(313, 324);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Time Filters";
            // 
            // labelMedianFilterRadius
            // 
            this.labelMedianFilterRadius.AutoSize = true;
            this.labelMedianFilterRadius.Location = new System.Drawing.Point(12, 178);
            this.labelMedianFilterRadius.Name = "labelMedianFilterRadius";
            this.labelMedianFilterRadius.Size = new System.Drawing.Size(40, 13);
            this.labelMedianFilterRadius.TabIndex = 28;
            this.labelMedianFilterRadius.Text = "Radius";
            // 
            // checkBoxMedianFilter
            // 
            this.checkBoxMedianFilter.AutoSize = true;
            this.checkBoxMedianFilter.Location = new System.Drawing.Point(6, 153);
            this.checkBoxMedianFilter.Name = "checkBoxMedianFilter";
            this.checkBoxMedianFilter.Size = new System.Drawing.Size(86, 17);
            this.checkBoxMedianFilter.TabIndex = 27;
            this.checkBoxMedianFilter.Text = "Median Filter";
            this.checkBoxMedianFilter.UseVisualStyleBackColor = true;
            // 
            // trackBarMedianFilterRadius
            // 
            this.trackBarMedianFilterRadius.AutoSize = false;
            this.trackBarMedianFilterRadius.LargeChange = 1;
            this.trackBarMedianFilterRadius.Location = new System.Drawing.Point(104, 176);
            this.trackBarMedianFilterRadius.Minimum = 1;
            this.trackBarMedianFilterRadius.Name = "trackBarMedianFilterRadius";
            this.trackBarMedianFilterRadius.Size = new System.Drawing.Size(193, 20);
            this.trackBarMedianFilterRadius.TabIndex = 26;
            this.trackBarMedianFilterRadius.Value = 5;
            // 
            // label2DCleanerThreshold
            // 
            this.label2DCleanerThreshold.AutoSize = true;
            this.label2DCleanerThreshold.Location = new System.Drawing.Point(12, 258);
            this.label2DCleanerThreshold.Name = "label2DCleanerThreshold";
            this.label2DCleanerThreshold.Size = new System.Drawing.Size(54, 13);
            this.label2DCleanerThreshold.TabIndex = 25;
            this.label2DCleanerThreshold.Text = "Threshold";
            // 
            // label2DCleanerRadius
            // 
            this.label2DCleanerRadius.AutoSize = true;
            this.label2DCleanerRadius.Location = new System.Drawing.Point(12, 236);
            this.label2DCleanerRadius.Name = "label2DCleanerRadius";
            this.label2DCleanerRadius.Size = new System.Drawing.Size(40, 13);
            this.label2DCleanerRadius.TabIndex = 24;
            this.label2DCleanerRadius.Text = "Radius";
            // 
            // trackBar2DCleanerThreshold
            // 
            this.trackBar2DCleanerThreshold.AutoSize = false;
            this.trackBar2DCleanerThreshold.Location = new System.Drawing.Point(104, 260);
            this.trackBar2DCleanerThreshold.Maximum = 255;
            this.trackBar2DCleanerThreshold.Minimum = 1;
            this.trackBar2DCleanerThreshold.Name = "trackBar2DCleanerThreshold";
            this.trackBar2DCleanerThreshold.Size = new System.Drawing.Size(193, 20);
            this.trackBar2DCleanerThreshold.TabIndex = 23;
            this.trackBar2DCleanerThreshold.Value = 5;
            // 
            // labelLinearAverageRadius
            // 
            this.labelLinearAverageRadius.AutoSize = true;
            this.labelLinearAverageRadius.Location = new System.Drawing.Point(11, 130);
            this.labelLinearAverageRadius.Name = "labelLinearAverageRadius";
            this.labelLinearAverageRadius.Size = new System.Drawing.Size(40, 13);
            this.labelLinearAverageRadius.TabIndex = 22;
            this.labelLinearAverageRadius.Text = "Radius";
            // 
            // checkBox2DCleaner
            // 
            this.checkBox2DCleaner.AutoSize = true;
            this.checkBox2DCleaner.Location = new System.Drawing.Point(6, 211);
            this.checkBox2DCleaner.Name = "checkBox2DCleaner";
            this.checkBox2DCleaner.Size = new System.Drawing.Size(79, 17);
            this.checkBox2DCleaner.TabIndex = 21;
            this.checkBox2DCleaner.Text = "2D Cleaner";
            this.checkBox2DCleaner.UseVisualStyleBackColor = true;
            // 
            // trackBar2DCleanerRadius
            // 
            this.trackBar2DCleanerRadius.AutoSize = false;
            this.trackBar2DCleanerRadius.LargeChange = 1;
            this.trackBar2DCleanerRadius.Location = new System.Drawing.Point(104, 234);
            this.trackBar2DCleanerRadius.Minimum = 1;
            this.trackBar2DCleanerRadius.Name = "trackBar2DCleanerRadius";
            this.trackBar2DCleanerRadius.Size = new System.Drawing.Size(193, 20);
            this.trackBar2DCleanerRadius.TabIndex = 20;
            this.trackBar2DCleanerRadius.Value = 5;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radioButtonProcessTypeParallelepiped);
            this.panel2.Controls.Add(this.radioButtonProcessTypePyramid);
            this.panel2.Controls.Add(this.radioButtonProcessTypeCone);
            this.panel2.Location = new System.Drawing.Point(86, 46);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(221, 22);
            this.panel2.TabIndex = 19;
            // 
            // radioButtonProcessTypeParallelepiped
            // 
            this.radioButtonProcessTypeParallelepiped.AutoSize = true;
            this.radioButtonProcessTypeParallelepiped.Checked = true;
            this.radioButtonProcessTypeParallelepiped.Location = new System.Drawing.Point(3, 3);
            this.radioButtonProcessTypeParallelepiped.Name = "radioButtonProcessTypeParallelepiped";
            this.radioButtonProcessTypeParallelepiped.Size = new System.Drawing.Size(91, 17);
            this.radioButtonProcessTypeParallelepiped.TabIndex = 13;
            this.radioButtonProcessTypeParallelepiped.TabStop = true;
            this.radioButtonProcessTypeParallelepiped.Text = "Parallelepiped";
            this.radioButtonProcessTypeParallelepiped.UseVisualStyleBackColor = true;
            // 
            // radioButtonProcessTypePyramid
            // 
            this.radioButtonProcessTypePyramid.AutoSize = true;
            this.radioButtonProcessTypePyramid.Location = new System.Drawing.Point(95, 3);
            this.radioButtonProcessTypePyramid.Name = "radioButtonProcessTypePyramid";
            this.radioButtonProcessTypePyramid.Size = new System.Drawing.Size(62, 17);
            this.radioButtonProcessTypePyramid.TabIndex = 14;
            this.radioButtonProcessTypePyramid.Text = "Pyramid";
            this.radioButtonProcessTypePyramid.UseVisualStyleBackColor = true;
            // 
            // radioButtonProcessTypeCone
            // 
            this.radioButtonProcessTypeCone.AutoSize = true;
            this.radioButtonProcessTypeCone.Location = new System.Drawing.Point(161, 3);
            this.radioButtonProcessTypeCone.Name = "radioButtonProcessTypeCone";
            this.radioButtonProcessTypeCone.Size = new System.Drawing.Size(50, 17);
            this.radioButtonProcessTypeCone.TabIndex = 15;
            this.radioButtonProcessTypeCone.Text = "Cone";
            this.radioButtonProcessTypeCone.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButtonColorModelYuv);
            this.panel1.Controls.Add(this.radioButtonColorModelRgb);
            this.panel1.Location = new System.Drawing.Point(86, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(221, 21);
            this.panel1.TabIndex = 18;
            // 
            // labelFrameNumber
            // 
            this.labelFrameNumber.AutoSize = true;
            this.labelFrameNumber.Location = new System.Drawing.Point(8, 74);
            this.labelFrameNumber.Name = "labelFrameNumber";
            this.labelFrameNumber.Size = new System.Drawing.Size(74, 13);
            this.labelFrameNumber.TabIndex = 17;
            this.labelFrameNumber.Text = "Frame number";
            // 
            // trackBarFrameNumber
            // 
            this.trackBarFrameNumber.AutoSize = false;
            this.trackBarFrameNumber.LargeChange = 1;
            this.trackBarFrameNumber.Location = new System.Drawing.Point(104, 75);
            this.trackBarFrameNumber.Maximum = 5;
            this.trackBarFrameNumber.Minimum = 1;
            this.trackBarFrameNumber.Name = "trackBarFrameNumber";
            this.trackBarFrameNumber.Size = new System.Drawing.Size(193, 20);
            this.trackBarFrameNumber.TabIndex = 16;
            this.trackBarFrameNumber.Value = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Process type";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Color model";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(331, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(397, 432);
            this.tabControl1.TabIndex = 12;
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(389, 406);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(389, 406);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // trackBar1
            // 
            this.trackBar1.AutoSize = false;
            this.trackBar1.Location = new System.Drawing.Point(6, 52);
            this.trackBar1.Maximum = 40;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(311, 20);
            this.trackBar1.TabIndex = 0;
            this.trackBar1.Value = 10;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(6, 6);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(78, 17);
            this.checkBox2.TabIndex = 4;
            this.checkBox2.Text = "Gray Scale";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // trackBar2
            // 
            this.trackBar2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar2.AutoSize = false;
            this.trackBar2.Location = new System.Drawing.Point(6, 100);
            this.trackBar2.Maximum = 70;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(419, 20);
            this.trackBar2.TabIndex = 5;
            this.trackBar2.Value = 50;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(5, 77);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(95, 17);
            this.checkBox3.TabIndex = 6;
            this.checkBox3.Text = "Log Correction";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.checkBox4);
            this.groupBox2.Controls.Add(this.trackBar3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.trackBar4);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.checkBox5);
            this.groupBox2.Controls.Add(this.trackBar5);
            this.groupBox2.Controls.Add(this.panel3);
            this.groupBox2.Controls.Add(this.panel4);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.trackBar6);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.checkBox6);
            this.groupBox2.Controls.Add(this.trackBar7);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 103);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(420, 329);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Time Filters";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Radius";
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(6, 153);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(86, 17);
            this.checkBox4.TabIndex = 27;
            this.checkBox4.Text = "Median Filter";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // trackBar3
            // 
            this.trackBar3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar3.AutoSize = false;
            this.trackBar3.LargeChange = 1;
            this.trackBar3.Location = new System.Drawing.Point(104, 176);
            this.trackBar3.Minimum = 1;
            this.trackBar3.Name = "trackBar3";
            this.trackBar3.Size = new System.Drawing.Size(300, 20);
            this.trackBar3.TabIndex = 26;
            this.trackBar3.Value = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 258);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Threshold";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 236);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Radius";
            // 
            // trackBar4
            // 
            this.trackBar4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar4.AutoSize = false;
            this.trackBar4.Location = new System.Drawing.Point(104, 260);
            this.trackBar4.Maximum = 255;
            this.trackBar4.Minimum = 1;
            this.trackBar4.Name = "trackBar4";
            this.trackBar4.Size = new System.Drawing.Size(300, 20);
            this.trackBar4.TabIndex = 23;
            this.trackBar4.Value = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 130);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Radius";
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(6, 211);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(79, 17);
            this.checkBox5.TabIndex = 21;
            this.checkBox5.Text = "2D Cleaner";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // trackBar5
            // 
            this.trackBar5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar5.AutoSize = false;
            this.trackBar5.LargeChange = 1;
            this.trackBar5.Location = new System.Drawing.Point(104, 234);
            this.trackBar5.Minimum = 1;
            this.trackBar5.Name = "trackBar5";
            this.trackBar5.Size = new System.Drawing.Size(300, 20);
            this.trackBar5.TabIndex = 20;
            this.trackBar5.Value = 5;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.radioButton1);
            this.panel3.Controls.Add(this.radioButton2);
            this.panel3.Controls.Add(this.radioButton3);
            this.panel3.Location = new System.Drawing.Point(86, 46);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(221, 22);
            this.panel3.TabIndex = 19;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(3, 3);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(91, 17);
            this.radioButton1.TabIndex = 13;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Parallelepiped";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(95, 3);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(62, 17);
            this.radioButton2.TabIndex = 14;
            this.radioButton2.Text = "Pyramid";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(161, 3);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(50, 17);
            this.radioButton3.TabIndex = 15;
            this.radioButton3.Text = "Cone";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.radioButton4);
            this.panel4.Controls.Add(this.radioButton5);
            this.panel4.Location = new System.Drawing.Point(86, 19);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(221, 21);
            this.panel4.TabIndex = 18;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(58, 3);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(47, 17);
            this.radioButton4.TabIndex = 10;
            this.radioButton4.Text = "YUV";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Checked = true;
            this.radioButton5.Location = new System.Drawing.Point(3, 3);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(48, 17);
            this.radioButton5.TabIndex = 9;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "RGB";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Frame number";
            // 
            // trackBar6
            // 
            this.trackBar6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar6.AutoSize = false;
            this.trackBar6.LargeChange = 1;
            this.trackBar6.Location = new System.Drawing.Point(104, 75);
            this.trackBar6.Maximum = 5;
            this.trackBar6.Minimum = 1;
            this.trackBar6.Name = "trackBar6";
            this.trackBar6.Size = new System.Drawing.Size(300, 20);
            this.trackBar6.TabIndex = 16;
            this.trackBar6.Value = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Process type";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Color model";
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(6, 107);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(98, 17);
            this.checkBox6.TabIndex = 8;
            this.checkBox6.Text = "Linear Average";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // trackBar7
            // 
            this.trackBar7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar7.AutoSize = false;
            this.trackBar7.LargeChange = 1;
            this.trackBar7.Location = new System.Drawing.Point(104, 130);
            this.trackBar7.Minimum = 1;
            this.trackBar7.Name = "trackBar7";
            this.trackBar7.Size = new System.Drawing.Size(300, 20);
            this.trackBar7.TabIndex = 7;
            this.trackBar7.Value = 5;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage8);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(434, 461);
            this.tabControl2.TabIndex = 12;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Controls.Add(this.panel8);
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(426, 435);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Filters";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.checkBox2);
            this.panel8.Controls.Add(this.trackBarLogCorrection);
            this.panel8.Controls.Add(this.checkBoxLogCorrection);
            this.panel8.Controls.Add(this.trackBarGammaCorrection);
            this.panel8.Controls.Add(this.checkBoxGammaCorrection);
            this.panel8.Controls.Add(this.trackBar2);
            this.panel8.Controls.Add(this.checkBox3);
            this.panel8.Controls.Add(this.checkBoxGrayScale);
            this.panel8.Controls.Add(this.trackBar1);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(420, 100);
            this.panel8.TabIndex = 12;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tabControl3);
            this.tabPage4.Controls.Add(this.panel5);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(426, 435);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "MotionDetectors";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage5);
            this.tabControl3.Controls.Add(this.tabPage6);
            this.tabControl3.Controls.Add(this.tabPage7);
            this.tabControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl3.Location = new System.Drawing.Point(3, 139);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(420, 293);
            this.tabControl3.TabIndex = 24;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.panel6);
            this.tabPage5.Controls.Add(this.chartMotionDetector);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(412, 267);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "MotionDetectors";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.Controls.Add(this.checkBoxMaxAreaColumn);
            this.panel6.Controls.Add(this.checkBoxMinAreaColumn);
            this.panel6.Controls.Add(this.checkBoxObjectCountColumn);
            this.panel6.Location = new System.Drawing.Point(301, 182);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(94, 74);
            this.panel6.TabIndex = 5;
            // 
            // checkBoxMaxAreaColumn
            // 
            this.checkBoxMaxAreaColumn.AutoSize = true;
            this.checkBoxMaxAreaColumn.Checked = true;
            this.checkBoxMaxAreaColumn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMaxAreaColumn.Location = new System.Drawing.Point(4, 50);
            this.checkBoxMaxAreaColumn.Name = "checkBoxMaxAreaColumn";
            this.checkBoxMaxAreaColumn.Size = new System.Drawing.Size(71, 17);
            this.checkBoxMaxAreaColumn.TabIndex = 2;
            this.checkBoxMaxAreaColumn.Text = "Max Area";
            this.checkBoxMaxAreaColumn.UseVisualStyleBackColor = true;
            // 
            // checkBoxMinAreaColumn
            // 
            this.checkBoxMinAreaColumn.AutoSize = true;
            this.checkBoxMinAreaColumn.Checked = true;
            this.checkBoxMinAreaColumn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMinAreaColumn.Location = new System.Drawing.Point(4, 27);
            this.checkBoxMinAreaColumn.Name = "checkBoxMinAreaColumn";
            this.checkBoxMinAreaColumn.Size = new System.Drawing.Size(67, 17);
            this.checkBoxMinAreaColumn.TabIndex = 1;
            this.checkBoxMinAreaColumn.Text = "Min area";
            this.checkBoxMinAreaColumn.UseVisualStyleBackColor = true;
            // 
            // checkBoxObjectCountColumn
            // 
            this.checkBoxObjectCountColumn.AutoSize = true;
            this.checkBoxObjectCountColumn.Checked = true;
            this.checkBoxObjectCountColumn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxObjectCountColumn.Location = new System.Drawing.Point(4, 4);
            this.checkBoxObjectCountColumn.Name = "checkBoxObjectCountColumn";
            this.checkBoxObjectCountColumn.Size = new System.Drawing.Size(88, 17);
            this.checkBoxObjectCountColumn.TabIndex = 0;
            this.checkBoxObjectCountColumn.Text = "Object Count";
            this.checkBoxObjectCountColumn.UseVisualStyleBackColor = true;
            // 
            // chartMotionDetector
            // 
            chartArea1.CursorX.IsUserEnabled = true;
            chartArea1.CursorX.IsUserSelectionEnabled = true;
            chartArea1.Name = "ChartArea1";
            this.chartMotionDetector.ChartAreas.Add(chartArea1);
            this.chartMotionDetector.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.chartMotionDetector.Legends.Add(legend1);
            this.chartMotionDetector.Location = new System.Drawing.Point(3, 3);
            this.chartMotionDetector.Name = "chartMotionDetector";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Object Count";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Min area";
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series2.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Max area";
            series3.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series3.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            this.chartMotionDetector.Series.Add(series1);
            this.chartMotionDetector.Series.Add(series2);
            this.chartMotionDetector.Series.Add(series3);
            this.chartMotionDetector.Size = new System.Drawing.Size(406, 261);
            this.chartMotionDetector.TabIndex = 2;
            this.chartMotionDetector.Text = "chartMotionDetector";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.panel7);
            this.tabPage6.Controls.Add(this.chartFeaturePoints);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(412, 267);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "FeaturePoints";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.Controls.Add(this.checkBoxSimilarPointColumn);
            this.panel7.Controls.Add(this.checkBoxFeaturePointColumn);
            this.panel7.Location = new System.Drawing.Point(308, 206);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(98, 53);
            this.panel7.TabIndex = 6;
            // 
            // checkBoxSimilarPointColumn
            // 
            this.checkBoxSimilarPointColumn.AutoSize = true;
            this.checkBoxSimilarPointColumn.Checked = true;
            this.checkBoxSimilarPointColumn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSimilarPointColumn.Location = new System.Drawing.Point(4, 30);
            this.checkBoxSimilarPointColumn.Name = "checkBoxSimilarPointColumn";
            this.checkBoxSimilarPointColumn.Size = new System.Drawing.Size(87, 17);
            this.checkBoxSimilarPointColumn.TabIndex = 1;
            this.checkBoxSimilarPointColumn.Text = "Similar points";
            this.checkBoxSimilarPointColumn.UseVisualStyleBackColor = true;
            // 
            // checkBoxFeaturePointColumn
            // 
            this.checkBoxFeaturePointColumn.AutoSize = true;
            this.checkBoxFeaturePointColumn.Checked = true;
            this.checkBoxFeaturePointColumn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFeaturePointColumn.Location = new System.Drawing.Point(4, 7);
            this.checkBoxFeaturePointColumn.Name = "checkBoxFeaturePointColumn";
            this.checkBoxFeaturePointColumn.Size = new System.Drawing.Size(94, 17);
            this.checkBoxFeaturePointColumn.TabIndex = 0;
            this.checkBoxFeaturePointColumn.Text = "Feature Points";
            this.checkBoxFeaturePointColumn.UseVisualStyleBackColor = true;
            // 
            // chartFeaturePoints
            // 
            chartArea2.CursorX.IsUserEnabled = true;
            chartArea2.CursorX.IsUserSelectionEnabled = true;
            chartArea2.Name = "ChartArea1";
            this.chartFeaturePoints.ChartAreas.Add(chartArea2);
            this.chartFeaturePoints.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Name = "Legend1";
            this.chartFeaturePoints.Legends.Add(legend2);
            this.chartFeaturePoints.Location = new System.Drawing.Point(3, 3);
            this.chartFeaturePoints.Name = "chartFeaturePoints";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Feature point Count";
            series4.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series4.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series5.ChartArea = "ChartArea1";
            series5.Legend = "Legend1";
            series5.Name = "Similar point count";
            series5.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series5.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            this.chartFeaturePoints.Series.Add(series4);
            this.chartFeaturePoints.Series.Add(series5);
            this.chartFeaturePoints.Size = new System.Drawing.Size(406, 261);
            this.chartFeaturePoints.TabIndex = 3;
            this.chartFeaturePoints.Text = "chartFeaturePoints";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.textBoxLog);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(412, 267);
            this.tabPage7.TabIndex = 2;
            this.tabPage7.Text = "Log";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // textBoxLog
            // 
            this.textBoxLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxLog.Location = new System.Drawing.Point(0, 0);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.Size = new System.Drawing.Size(412, 267);
            this.textBoxLog.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.labelBlockThreshold);
            this.panel5.Controls.Add(this.labelBlockSize);
            this.panel5.Controls.Add(this.trackBarBlockThreshold);
            this.panel5.Controls.Add(this.trackBarBlockSize);
            this.panel5.Controls.Add(this.checkBoxBlockMatchingDetector);
            this.panel5.Controls.Add(this.checkBoxLog);
            this.panel5.Controls.Add(this.checkBoxBackgroundSubtraction);
            this.panel5.Controls.Add(this.trackBarBackgroundSubstractorThreshold);
            this.panel5.Controls.Add(this.checkBoxFeatureMotionDetector);
            this.panel5.Controls.Add(this.labelBackroundSubstractorThreshold);
            this.panel5.Controls.Add(this.labelSceneChangeDetectorThreshold);
            this.panel5.Controls.Add(this.checkBoxSceneChangeDetector);
            this.panel5.Controls.Add(this.trackBarSceneChangeDetectorThreshold);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(420, 136);
            this.panel5.TabIndex = 25;
            // 
            // labelBlockThreshold
            // 
            this.labelBlockThreshold.AutoSize = true;
            this.labelBlockThreshold.Location = new System.Drawing.Point(153, 106);
            this.labelBlockThreshold.Name = "labelBlockThreshold";
            this.labelBlockThreshold.Size = new System.Drawing.Size(54, 13);
            this.labelBlockThreshold.TabIndex = 29;
            this.labelBlockThreshold.Text = "Threshold";
            // 
            // labelBlockSize
            // 
            this.labelBlockSize.AutoSize = true;
            this.labelBlockSize.Location = new System.Drawing.Point(153, 84);
            this.labelBlockSize.Name = "labelBlockSize";
            this.labelBlockSize.Size = new System.Drawing.Size(54, 13);
            this.labelBlockSize.TabIndex = 28;
            this.labelBlockSize.Text = "BlockSize";
            // 
            // trackBarBlockThreshold
            // 
            this.trackBarBlockThreshold.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarBlockThreshold.AutoSize = false;
            this.trackBarBlockThreshold.LargeChange = 1;
            this.trackBarBlockThreshold.Location = new System.Drawing.Point(239, 106);
            this.trackBarBlockThreshold.Maximum = 200;
            this.trackBarBlockThreshold.Minimum = 10;
            this.trackBarBlockThreshold.Name = "trackBarBlockThreshold";
            this.trackBarBlockThreshold.Size = new System.Drawing.Size(165, 20);
            this.trackBarBlockThreshold.TabIndex = 27;
            this.trackBarBlockThreshold.Value = 50;
            // 
            // trackBarBlockSize
            // 
            this.trackBarBlockSize.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarBlockSize.AutoSize = false;
            this.trackBarBlockSize.LargeChange = 1;
            this.trackBarBlockSize.Location = new System.Drawing.Point(239, 80);
            this.trackBarBlockSize.Maximum = 50;
            this.trackBarBlockSize.Minimum = 3;
            this.trackBarBlockSize.Name = "trackBarBlockSize";
            this.trackBarBlockSize.Size = new System.Drawing.Size(165, 20);
            this.trackBarBlockSize.TabIndex = 26;
            this.trackBarBlockSize.Value = 15;
            // 
            // checkBoxBlockMatchingDetector
            // 
            this.checkBoxBlockMatchingDetector.AutoSize = true;
            this.checkBoxBlockMatchingDetector.Location = new System.Drawing.Point(3, 83);
            this.checkBoxBlockMatchingDetector.Name = "checkBoxBlockMatchingDetector";
            this.checkBoxBlockMatchingDetector.Size = new System.Drawing.Size(144, 17);
            this.checkBoxBlockMatchingDetector.TabIndex = 25;
            this.checkBoxBlockMatchingDetector.Text = "Block Matching Detector";
            this.checkBoxBlockMatchingDetector.UseVisualStyleBackColor = true;
            // 
            // checkBoxLog
            // 
            this.checkBoxLog.AutoSize = true;
            this.checkBoxLog.Checked = true;
            this.checkBoxLog.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxLog.Location = new System.Drawing.Point(4, 109);
            this.checkBoxLog.Name = "checkBoxLog";
            this.checkBoxLog.Size = new System.Drawing.Size(58, 17);
            this.checkBoxLog.TabIndex = 24;
            this.checkBoxLog.Text = "Loging";
            this.checkBoxLog.UseVisualStyleBackColor = true;
            // 
            // checkBoxBackgroundSubtraction
            // 
            this.checkBoxBackgroundSubtraction.AutoSize = true;
            this.checkBoxBackgroundSubtraction.Location = new System.Drawing.Point(3, 11);
            this.checkBoxBackgroundSubtraction.Name = "checkBoxBackgroundSubtraction";
            this.checkBoxBackgroundSubtraction.Size = new System.Drawing.Size(141, 17);
            this.checkBoxBackgroundSubtraction.TabIndex = 5;
            this.checkBoxBackgroundSubtraction.Text = "Background Subtraction";
            this.checkBoxBackgroundSubtraction.UseVisualStyleBackColor = true;
            // 
            // trackBarBackgroundSubstractorThreshold
            // 
            this.trackBarBackgroundSubstractorThreshold.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarBackgroundSubstractorThreshold.AutoSize = false;
            this.trackBarBackgroundSubstractorThreshold.LargeChange = 1;
            this.trackBarBackgroundSubstractorThreshold.Location = new System.Drawing.Point(239, 11);
            this.trackBarBackgroundSubstractorThreshold.Maximum = 300;
            this.trackBarBackgroundSubstractorThreshold.Minimum = 10;
            this.trackBarBackgroundSubstractorThreshold.Name = "trackBarBackgroundSubstractorThreshold";
            this.trackBarBackgroundSubstractorThreshold.Size = new System.Drawing.Size(165, 20);
            this.trackBarBackgroundSubstractorThreshold.TabIndex = 18;
            this.trackBarBackgroundSubstractorThreshold.Value = 70;
            // 
            // checkBoxFeatureMotionDetector
            // 
            this.checkBoxFeatureMotionDetector.AutoSize = true;
            this.checkBoxFeatureMotionDetector.Location = new System.Drawing.Point(3, 60);
            this.checkBoxFeatureMotionDetector.Name = "checkBoxFeatureMotionDetector";
            this.checkBoxFeatureMotionDetector.Size = new System.Drawing.Size(141, 17);
            this.checkBoxFeatureMotionDetector.TabIndex = 23;
            this.checkBoxFeatureMotionDetector.Text = "Feature Motion Detector";
            this.checkBoxFeatureMotionDetector.UseVisualStyleBackColor = true;
            // 
            // labelBackroundSubstractorThreshold
            // 
            this.labelBackroundSubstractorThreshold.AutoSize = true;
            this.labelBackroundSubstractorThreshold.Location = new System.Drawing.Point(149, 13);
            this.labelBackroundSubstractorThreshold.Name = "labelBackroundSubstractorThreshold";
            this.labelBackroundSubstractorThreshold.Size = new System.Drawing.Size(54, 13);
            this.labelBackroundSubstractorThreshold.TabIndex = 19;
            this.labelBackroundSubstractorThreshold.Text = "Threshold";
            // 
            // labelSceneChangeDetectorThreshold
            // 
            this.labelSceneChangeDetectorThreshold.AutoSize = true;
            this.labelSceneChangeDetectorThreshold.Location = new System.Drawing.Point(149, 41);
            this.labelSceneChangeDetectorThreshold.Name = "labelSceneChangeDetectorThreshold";
            this.labelSceneChangeDetectorThreshold.Size = new System.Drawing.Size(54, 13);
            this.labelSceneChangeDetectorThreshold.TabIndex = 22;
            this.labelSceneChangeDetectorThreshold.Text = "Threshold";
            // 
            // checkBoxSceneChangeDetector
            // 
            this.checkBoxSceneChangeDetector.AutoSize = true;
            this.checkBoxSceneChangeDetector.Location = new System.Drawing.Point(3, 37);
            this.checkBoxSceneChangeDetector.Name = "checkBoxSceneChangeDetector";
            this.checkBoxSceneChangeDetector.Size = new System.Drawing.Size(141, 17);
            this.checkBoxSceneChangeDetector.TabIndex = 20;
            this.checkBoxSceneChangeDetector.Text = "Scene Change Detector";
            this.checkBoxSceneChangeDetector.UseVisualStyleBackColor = true;
            // 
            // trackBarSceneChangeDetectorThreshold
            // 
            this.trackBarSceneChangeDetectorThreshold.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarSceneChangeDetectorThreshold.AutoSize = false;
            this.trackBarSceneChangeDetectorThreshold.LargeChange = 1;
            this.trackBarSceneChangeDetectorThreshold.Location = new System.Drawing.Point(239, 41);
            this.trackBarSceneChangeDetectorThreshold.Maximum = 50;
            this.trackBarSceneChangeDetectorThreshold.Minimum = 1;
            this.trackBarSceneChangeDetectorThreshold.Name = "trackBarSceneChangeDetectorThreshold";
            this.trackBarSceneChangeDetectorThreshold.Size = new System.Drawing.Size(165, 20);
            this.trackBarSceneChangeDetectorThreshold.TabIndex = 21;
            this.trackBarSceneChangeDetectorThreshold.Value = 8;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.checkBoxUsingColorInfromation);
            this.tabPage8.Controls.Add(this.checkBoxVocrContourShow);
            this.tabPage8.Controls.Add(this.trackBarVocrControurGain);
            this.tabPage8.Controls.Add(this.labelVocrControurGain);
            this.tabPage8.Controls.Add(this.checkBoxVocrContour);
            this.tabPage8.Controls.Add(this.trackBarVocrControurBrightnessThreshold);
            this.tabPage8.Controls.Add(this.labelVocrControurBrightnessThreshold);
            this.tabPage8.Controls.Add(this.trackBarVocrControurThreshold);
            this.tabPage8.Controls.Add(this.labelVocrControurThreshold);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(426, 435);
            this.tabPage8.TabIndex = 2;
            this.tabPage8.Text = "VOCR";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // trackBarVocrControurGain
            // 
            this.trackBarVocrControurGain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarVocrControurGain.AutoSize = false;
            this.trackBarVocrControurGain.LargeChange = 1;
            this.trackBarVocrControurGain.Location = new System.Drawing.Point(136, 61);
            this.trackBarVocrControurGain.Maximum = 1000;
            this.trackBarVocrControurGain.Minimum = 1;
            this.trackBarVocrControurGain.Name = "trackBarVocrControurGain";
            this.trackBarVocrControurGain.Size = new System.Drawing.Size(282, 20);
            this.trackBarVocrControurGain.TabIndex = 23;
            this.trackBarVocrControurGain.Value = 60;
            // 
            // labelVocrControurGain
            // 
            this.labelVocrControurGain.AutoSize = true;
            this.labelVocrControurGain.Location = new System.Drawing.Point(7, 63);
            this.labelVocrControurGain.Name = "labelVocrControurGain";
            this.labelVocrControurGain.Size = new System.Drawing.Size(29, 13);
            this.labelVocrControurGain.TabIndex = 24;
            this.labelVocrControurGain.Tag = "";
            this.labelVocrControurGain.Text = "Gain";
            // 
            // checkBoxVocrContour
            // 
            this.checkBoxVocrContour.AutoSize = true;
            this.checkBoxVocrContour.Location = new System.Drawing.Point(8, 12);
            this.checkBoxVocrContour.Name = "checkBoxVocrContour";
            this.checkBoxVocrContour.Size = new System.Drawing.Size(117, 17);
            this.checkBoxVocrContour.TabIndex = 20;
            this.checkBoxVocrContour.Text = "Contour processing";
            this.checkBoxVocrContour.UseVisualStyleBackColor = true;
            // 
            // trackBarVocrControurBrightnessThreshold
            // 
            this.trackBarVocrControurBrightnessThreshold.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarVocrControurBrightnessThreshold.AutoSize = false;
            this.trackBarVocrControurBrightnessThreshold.LargeChange = 1;
            this.trackBarVocrControurBrightnessThreshold.Location = new System.Drawing.Point(136, 87);
            this.trackBarVocrControurBrightnessThreshold.Maximum = 1000;
            this.trackBarVocrControurBrightnessThreshold.Minimum = 10;
            this.trackBarVocrControurBrightnessThreshold.Name = "trackBarVocrControurBrightnessThreshold";
            this.trackBarVocrControurBrightnessThreshold.Size = new System.Drawing.Size(282, 20);
            this.trackBarVocrControurBrightnessThreshold.TabIndex = 21;
            this.trackBarVocrControurBrightnessThreshold.Value = 200;
            // 
            // labelVocrControurBrightnessThreshold
            // 
            this.labelVocrControurBrightnessThreshold.AutoSize = true;
            this.labelVocrControurBrightnessThreshold.Location = new System.Drawing.Point(7, 89);
            this.labelVocrControurBrightnessThreshold.Name = "labelVocrControurBrightnessThreshold";
            this.labelVocrControurBrightnessThreshold.Size = new System.Drawing.Size(102, 13);
            this.labelVocrControurBrightnessThreshold.TabIndex = 22;
            this.labelVocrControurBrightnessThreshold.Text = "Brightness threshold";
            // 
            // trackBarVocrControurThreshold
            // 
            this.trackBarVocrControurThreshold.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarVocrControurThreshold.AutoSize = false;
            this.trackBarVocrControurThreshold.LargeChange = 1;
            this.trackBarVocrControurThreshold.Location = new System.Drawing.Point(136, 35);
            this.trackBarVocrControurThreshold.Maximum = 1000;
            this.trackBarVocrControurThreshold.Minimum = 10;
            this.trackBarVocrControurThreshold.Name = "trackBarVocrControurThreshold";
            this.trackBarVocrControurThreshold.Size = new System.Drawing.Size(282, 20);
            this.trackBarVocrControurThreshold.TabIndex = 21;
            this.trackBarVocrControurThreshold.Value = 800;
            // 
            // labelVocrControurThreshold
            // 
            this.labelVocrControurThreshold.AutoSize = true;
            this.labelVocrControurThreshold.Location = new System.Drawing.Point(7, 37);
            this.labelVocrControurThreshold.Name = "labelVocrControurThreshold";
            this.labelVocrControurThreshold.Size = new System.Drawing.Size(54, 13);
            this.labelVocrControurThreshold.TabIndex = 22;
            this.labelVocrControurThreshold.Text = "Threshold";
            // 
            // checkBoxVocrContourShow
            // 
            this.checkBoxVocrContourShow.AutoSize = true;
            this.checkBoxVocrContourShow.Location = new System.Drawing.Point(10, 115);
            this.checkBoxVocrContourShow.Name = "checkBoxVocrContourShow";
            this.checkBoxVocrContourShow.Size = new System.Drawing.Size(95, 17);
            this.checkBoxVocrContourShow.TabIndex = 25;
            this.checkBoxVocrContourShow.Text = "Show controur";
            this.checkBoxVocrContourShow.UseVisualStyleBackColor = true;
            // 
            // checkBoxUsingColorInfromation
            // 
            this.checkBoxUsingColorInfromation.AutoSize = true;
            this.checkBoxUsingColorInfromation.Location = new System.Drawing.Point(10, 155);
            this.checkBoxUsingColorInfromation.Name = "checkBoxUsingColorInfromation";
            this.checkBoxUsingColorInfromation.Size = new System.Drawing.Size(133, 17);
            this.checkBoxUsingColorInfromation.TabIndex = 26;
            this.checkBoxUsingColorInfromation.Text = "Using color information";
            this.checkBoxUsingColorInfromation.UseVisualStyleBackColor = true;
            // 
            // EffectsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 461);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this.tabControl1);
            this.MinimumSize = new System.Drawing.Size(450, 500);
            this.Name = "EffectsForm";
            this.Text = "Video Effects";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FilterForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarGammaCorrection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarLogCorrection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarLinearAverageRadius)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarMedianFilterRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2DCleanerThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2DCleanerRadius)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarFrameNumber)).EndInit();
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar5)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar7)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartMotionDetector)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartFeaturePoints)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarBlockThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarBlockSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarBackgroundSubstractorThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSceneChangeDetectorThreshold)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVocrControurGain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVocrControurBrightnessThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVocrControurThreshold)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxGammaCorrection;
        private System.Windows.Forms.CheckBox checkBoxGrayScale;
        private System.Windows.Forms.TrackBar trackBarGammaCorrection;
        private System.Windows.Forms.CheckBox checkBoxLogCorrection;
        private System.Windows.Forms.TrackBar trackBarLogCorrection;
        private System.Windows.Forms.CheckBox checkBoxLinearAverage;
        private System.Windows.Forms.TrackBar trackBarLinearAverageRadius;
        private System.Windows.Forms.RadioButton radioButtonColorModelRgb;
        private System.Windows.Forms.RadioButton radioButtonColorModelYuv;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonProcessTypeCone;
        private System.Windows.Forms.RadioButton radioButtonProcessTypePyramid;
        private System.Windows.Forms.RadioButton radioButtonProcessTypeParallelepiped;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelFrameNumber;
        private System.Windows.Forms.TrackBar trackBarFrameNumber;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox checkBox2DCleaner;
        private System.Windows.Forms.TrackBar trackBar2DCleanerRadius;
        private System.Windows.Forms.Label labelLinearAverageRadius;
        private System.Windows.Forms.Label label2DCleanerThreshold;
        private System.Windows.Forms.Label label2DCleanerRadius;
        private System.Windows.Forms.TrackBar trackBar2DCleanerThreshold;
        private System.Windows.Forms.Label labelMedianFilterRadius;
        private System.Windows.Forms.CheckBox checkBoxMedianFilter;
        private System.Windows.Forms.TrackBar trackBarMedianFilterRadius;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.TrackBar trackBar3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TrackBar trackBar4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.TrackBar trackBar5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TrackBar trackBar6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.TrackBar trackBar7;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label labelBackroundSubstractorThreshold;
        private System.Windows.Forms.TrackBar trackBarBackgroundSubstractorThreshold;
        private System.Windows.Forms.CheckBox checkBoxBackgroundSubtraction;
        private System.Windows.Forms.Label labelSceneChangeDetectorThreshold;
        private System.Windows.Forms.TrackBar trackBarSceneChangeDetectorThreshold;
        private System.Windows.Forms.CheckBox checkBoxSceneChangeDetector;
        private System.Windows.Forms.CheckBox checkBoxFeatureMotionDetector;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartMotionDetector;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartFeaturePoints;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.CheckBox checkBoxMaxAreaColumn;
        private System.Windows.Forms.CheckBox checkBoxMinAreaColumn;
        private System.Windows.Forms.CheckBox checkBoxObjectCountColumn;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.CheckBox checkBoxLog;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.CheckBox checkBoxSimilarPointColumn;
        private System.Windows.Forms.CheckBox checkBoxFeaturePointColumn;
        private System.Windows.Forms.CheckBox checkBoxBlockMatchingDetector;
        private System.Windows.Forms.Label labelBlockThreshold;
        private System.Windows.Forms.Label labelBlockSize;
        private System.Windows.Forms.TrackBar trackBarBlockThreshold;
        private System.Windows.Forms.TrackBar trackBarBlockSize;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TrackBar trackBarVocrControurGain;
        private System.Windows.Forms.Label labelVocrControurGain;
        private System.Windows.Forms.CheckBox checkBoxVocrContour;
        private System.Windows.Forms.TrackBar trackBarVocrControurThreshold;
        private System.Windows.Forms.Label labelVocrControurThreshold;
        private System.Windows.Forms.TrackBar trackBarVocrControurBrightnessThreshold;
        private System.Windows.Forms.Label labelVocrControurBrightnessThreshold;
        private System.Windows.Forms.CheckBox checkBoxVocrContourShow;
        private System.Windows.Forms.CheckBox checkBoxUsingColorInfromation;
    }
}